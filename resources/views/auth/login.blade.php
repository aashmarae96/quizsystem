@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color:#DF6C0E;color:white">{{ __('Login') }}</div>

                <div class="card-body" style="background-color:darkgray;color:white">
                    @if(session('status'))
                        <div class="alert alert-warning">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action={{Route('login')}}>
                        @csrf
                        <div>
                            <small class="text-danger">{{ $errors->first('message') }}</small>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="background-color:#DF6C0E;color:white ">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" style="background-color:#DF6C0E;color:white " href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>



{{--<div class="container loginPage">--}}
   {{--<div class="col-md-12 page">--}}
       {{--<div class="col-md-4" id="figure">--}}
         {{--<img class="logo" src="/img/umva.png">--}}
       {{--</div>--}}
       {{--<div class="col-md-8 form">--}}

           {{--<div class="textLogin">--}}
               {{--Welcome to AUX-EL login Panel--}}
           {{--</div>--}}
               {{--<form method="POST" action="/loginCheck">--}}
               {{--@csrf--}}
                   {{--<div>--}}
                   {{--<small class="text-danger">{{ $errors->first('message') }}</small>--}}
                   {{--</div>--}}
           {{--<div class="input-container language">--}}
               {{--<label class="text">Choose Language</label>--}}
                {{--<select name="language" class="form-control input-field">--}}

                           {{--<option value="English">English</option>--}}
                       {{--<option value="English">French</option>--}}
                       {{--<option value="English">Dutch</option>--}}



                   {{--</select>--}}

           {{--</div>--}}
           {{--<div class="card">--}}
               {{--<div class="card-header">Login</div>--}}
               {{--@if(session('status'))--}}
                   {{--<div class="alert alert-warning" style="color: red">--}}
                       {{--{{ session('status') }}--}}
                   {{--</div>--}}
               {{--@endif--}}
            {{--<div class="card-body">--}}
           {{--<div class="input-container">--}}
               {{--@if ($errors->has('name'))--}}
                   {{--<span class="invalid-feedback" role="alert">--}}
               {{--<strong>{{ $errors->first('name') }}</strong>--}}
               {{--</span>--}}
               {{--@endif--}}


               {{--<label class="text">Username</label>--}}
               {{--<input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}  input-field" placeholder="Username" name="name" value="{{ old('name') }}"  autofocus >--}}

           {{--</div>--}}
                {{--<div>--}}
                    {{--@if ($errors->has('name'))--}}
                        {{--<div class="error">{{ $errors->first('name') }}</div>--}}
                    {{--@endif--}}
                {{--</div>--}}

           {{--<div class="input-container">--}}
               {{--<label class="text">Password</label>--}}
               {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-field"  placeholder="Password" name="password" >--}}

           {{--</div>--}}
                {{--<div>--}}
                    {{--@if ($errors->has('password'))--}}
                        {{--<div class="error">{{ $errors->first('password') }}</div>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="loginButton">--}}
                    {{--<button type="submit" class="btn btn-primary buttons">Login</button>--}}
                {{--</div>--}}
           {{--</div>--}}
           {{--</div>--}}
           {{--<div class="card forget">--}}
               {{--<div class="card-header">Forgot your password?</div>--}}
               {{--<div class="card-body">--}}
                   {{--<a class="btn btn-link forgetbttn" href="{{ route('password.request') }}"> Forgot your password?</a>--}}

               {{--</div>--}}
           {{--</div>--}}
               {{--</form>--}}
            {{--</div>--}}
       {{--</div>--}}
   {{--</div>--}}
@endsection
