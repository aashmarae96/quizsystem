<html>
<head>
    <title>AUX-EL</title>
    <meta name="robots" content="noindex" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script src="{{asset('js/app.js')}}"></script>


    <style>
        .header {
            background-color: #DF6C0E;
            width: 100%;
            height: 20%;
            font-size: 40px;
            color:white;
            display:block;
        }
        @media screen and (max-width: 500px) {
            .header a {
                float: none;
                display: block;
                text-align: left;
                float:none;
                font-size: 40px;
            }

        }
       .logo{
           margin-top:0.5%;
           padding-left: 4%;
       }
        .auxel{
            margin-top:2%;
            padding-left: 6%;

        }
        .logout{
            margin-top:6%;
            padding-left:25%;
            font-size: 18px;

        }



        .nav{
            background-color:#8B3F00;
            width:100%;


        }

        a{
            color:white;


        }
        .img-container{
            width:150px;
            height:225px;
            position: relative;
            display: inline;
            margin:5px;

        }
        .image{
            height:90%;
        }
        .text{
            width:60%;
            height:60%;
        }
     /*showing course list*/
        #courses {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-top:-2%;
        }
        #courses td, #courses th {
            border: 1px solid #ddd;
            padding: 8px;
        }
        /*#courses tr:nth-child(even){background-color: #F8DDC2;}*/

        #courses tr:hover {background-color: #ddd;}

        #courses th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color:  #8B3F00;
            color: white;
        }

        /*styling of icons*/
        .fa-eye,.fa-eye-slash{
            padding-left:3%
        }
        .fa-pencil-square-o{
            padding-left:30%
        }
        /*Styling of home page*/
        .home{
            margin-top:2%;
        }
        .btn-default{
        background-color: #8B3F00;
            color:white;
        }
       /*Multistage*/

        /*custom font*/
        @import url(https://fonts.googleapis.com/css?family=Montserrat);

        /*basic reset*/
        * {
            margin: 0;
            padding: 0;
        }

        /*html {*/
            /*height: 100%;*/
            /*background: #6441A5; !* fallback for old browsers *!*/
            /*background: -webkit-linear-gradient(to left, #6441A5, #2a0845); !* Chrome 10-25, Safari 5.1-6 *!*/
        /*}*/

        body {
            font-family: montserrat, arial, verdana;
            background: transparent;
        }

        /*form styles*/
        #msform {
            text-align: center;
            position: relative;
            margin-top: 30px;
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
            padding: 20px 30px;
            box-sizing: border-box;
            width: 100%;
            margin: 0 10%;

            /*stacking fieldsets above each other*/
            position: relative;
        }

        /*Hide all except first fieldset*/
        #msform fieldset:not(:first-of-type) {
            display: none;
        }

        /*inputs*/
        #msform input, #msform textarea {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 13px;
        }

        #msform input:focus, #msform textarea:focus {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            border: 1px solid  #8B3F00;
            outline-width: 0;
            transition: All 0.5s ease-in;
            -webkit-transition: All 0.5s ease-in;
            -moz-transition: All 0.5s ease-in;
            -o-transition: All 0.5s ease-in;
        }

        /*buttons*/
        #msform .action-button {
            width: 100px;
            background: #8B3F00;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 25px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }

        #msform .action-button:hover, #msform .action-button:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #8B3F00;
        }

        #msform .action-button-previous {
            width: 100px;
            background: #C5C5F1;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 25px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }

        #msform .action-button-previous:hover, #msform .action-button-previous:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #C5C5F1;
        }

        /*headings*/
        .fs-title {
            font-size: 18px;
            text-transform: uppercase;
            color: #2C3E50;
            margin-bottom: 10px;
            letter-spacing: 2px;
            font-weight: bold;
        }

        .fs-subtitle {
            font-weight: normal;
            font-size: 13px;
            color: #666;
            margin-bottom: 20px;
        }

        /*progressbar*/
        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            /*CSS counters to number the steps*/
            counter-reset: step;
        }

        #progressbar li {
            list-style-type: none;
            color: white;
            text-transform: uppercase;
            font-size: 9px;
            width: 33.33%;
            float: left;
            position: relative;
            letter-spacing: 1px;
        }

        #progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 24px;
            height: 24px;
            line-height: 26px;
            display: block;
            font-size: 12px;
            color: #333;
            background: white;
            border-radius: 25px;
            margin: 0 auto 10px auto;
        }

        /*progressbar connectors*/
        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: white;
            position: absolute;
            left: -50%;
            top: 9px;
            z-index: -1; /*put it behind the numbers*/
        }

        #progressbar li:first-child:after {
            /*connector not needed before the first step*/
            content: none;
        }

        /*marking active/completed steps green*/
        /*The number of the step and the connector before it = green*/
        #progressbar li.active:before, #progressbar li.active:after {
            background: #8B3F00;
            color: white;
        }


        /* Not relevant to this form */
        .dme_link {
            margin-top: 30px;
            text-align: center;
        }
        .dme_link a {
            background: #FFF;
            font-weight: bold;
            color:  #8B3F00;
            border: 0 none;
            border-radius: 25px;
            cursor: pointer;
            padding: 5px 25px;
            font-size: 12px;
        }

        .dme_link a:hover, .dme_link a:focus {
            background: #C5C5F1;
            text-decoration: none;
        }
        div.welcome{
            background: #8B3F00;
            color:#ffffff;
            height:65%;
            border-radius:10px;



        }
        .banner {
            height: 65%;
        }

        .adddata{
            background-color: #F8DDC2;
            height:60%;




        }
        #container{
            min-height: 100%;
            position: relative;
        }

        html,body{
            height:100%;
        }

        /*.footer {*/
             /*!*position:absolute;*!*/
             /*!*left: 0;*!*/
             /*!*bottom: 0;*!*/
             /*!*width: 100%;*!*/
             /*!*height:8%;*!*/
             /*background-color: #8B3F00;*/
             /*color: white;*/
             /*text-align: center;*/
             /*clear: both;*/
             /*bottom: 0;*/
             /*width: 100%;*/
             /*position: absolute;*/


             /*height: 3em;*/
             /*margin-top: -3em;*/
         /*}*/
        /*.home{*/
            /*height:90%;*/
            /*padding-bottom: 3em;*/
            /*position: relative;*/
        /*}*/
        #container{
            min-height: 100%;
            margin-bottom: -50px;
            position: relative;
        }
        #footer{
            height: 50px;
            position: relative;
            background-color: #8B3F00;
            color: white;
            text-align: center;
        }
        .clearfooter {
            height: 50px;
            clear: both;
        }
        .buttons{
            margin-left: 3%;
        }
        .result{
            margin-left: 25%;
            margin-top: 3%;
            outline: 2px solid black;
            background-color: #F8DDC2;
            box-shadow: 10px 10px 5px grey;
            width:50%;
            height:60%;
        }
        .pdfbox{
            margin-top: 15%;
            margin-left: 15%;
           font-size: 18px;
        }

        .pdfbutton{
            margin-left: 30%;
        }
        .chapterlist{
          /*border:1px solid saddlebrown;*/
            padding-left:2%;

        }






    </style>


    </head>
<body>
<div id="container">
<div class="col-md-12">

<div class="header">


    <div class="col-md-4 auxel "><a href="/home"class="one">Quiz System</a></div>
    {{--<div class="col-md-4 logout"><a class="one" href="{{ route('logout') }}"--}}
                                    {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();"><small>{{$username}}</small><br><i class="fa fa-user fa-1g"></i>--}}
            {{--{{ __('Logout') }}--}}
        {{--</a>--}}
    <div class="col-md-4 logout"><a class="one" href="/logout"><small>{{Session::get('user')}}</small><br><i class="fa fa-user fa-1g"></i>
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</div>
</div>
<div class="col-md-12">
<div class="nav">
    <ul class="nav navbar-nav">
        <li ><a href="/home"><i class="fa fa-home "></i> HOME</a></li>
        <li><a href="/overview"><i class="fa fa-book "></i> COURSE OVERVIEW</a></li>


    </ul>

</div>
</div>
<div class="row col-md-12 home">

    {{--test--}}
    {{--{{dd(Auth::user()->name)}}--}}
        @yield('content')

</div>

</div>
<div class="clearfooter">

</div>

<div id="footer">
    <p>Copyright © 2019. All Rights Reserved</p>
</div>


</body>

<script>
    $( document ).ready(function() {

//        $(".answer").click(function () {
//            $('.answer').removeAttr('checked');
//            $(this).attr('checked', 'checked');
//            $('#checked_answer').val($(this).val());
//        })

        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets


        var left, opacity, scale; //fieldset properties which we will animate

        var animating; //flag to prevent quick multi-click glitches
        $(".next").click(function () {

             //alert('test');


//             console.log($('.answer:checked').length);
            var checkedValue = $('#checked_answer').val();
           var qid=$(this).attr('ref');
            var question = $('#question_'+qid).val();
          //  $('#question').val(question+1);
            var correct_id = $('#correct_id_'+qid).val();

            console.log("question_id",question);
            console.log("correct_id", correct_id);

            current_fs = $(this).parent();
            if (current_fs.find('input:radio:checked').length ==0  ) {
//
                document.getElementById("error").innerHTML ="Please select a radio button!!";
                console.log('TEST');
                return false;
            }
            else{
                document.getElementById("error").innerHTML ="";
            }
            if (animating) return false;
            animating = true;
            console.log("answer_id", "Answer");




            $('.answer').removeAttr('checked');

            current_fs = $(this).parent();
            next_fs=$(this).parent().next();




            //show the next fieldset

                next_fs.show();
//
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50) + "%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'position': 'absolute'
                        });
                        next_fs.css({'left': left, 'opacity': opacity});
                    },
                    duration: 800,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    // easing: 'easeInOutBack'
                });

                // $('#msform').submit();

        });


        $(".previous").click(function () {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //de-activate current step on progressbar
            //  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                //easing: 'easeInOutBack'
            });
        });

        $(".submit").click(function () {


        })
    });

//    function val() {
//        var ans = document.getElementById('data[]');
//        var hasChecked = false;
//        for (var i = 0; i < ans.length; i++) {
//
//            if (ans[i].checked) {
//                hasChecked = true;
//                break;
//
//            }
//        }
//        if(hasChecked==false){
//            alert("Please select one answer");
//            return false;
//        }
//        return true;
//
//    }


</script>
{{--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}
{{--<script>--}}
    {{--$(document).ready(function () {--}}
       {{--$('#courseTable').DataTable({--}}
           {{--processing: true,--}}
           {{--serverSide: true,--}}
           {{--ajax: '{!! route('getCourses') !!}',--}}
           {{--columns: [--}}
               {{--{ data: 'course_id', name: 'course_id' },--}}
               {{--{ data: 'title', name: 'title' }--}}
               {{--]--}}
        {{--});--}}

    {{--});--}}
    {{--</script>--}}


