<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        div.loginPage{
            width: 60%;
            padding: 10px;
            /*border:1px solid black;*/
            margin-left:20%;
            margin-top: 6%;
            height:200%;
            font-size:12px ;
            background-color: white;
            box-shadow: 10px 10px 5px grey;


        }
        .form{
            margin-left: 35%;
        }
        .logo{
            padding-top: 45%;
            padding-left: 20%;
        }

        .textLogin{
            border-bottom: 1px solid lightgray;
            margin-bottom: 1%;
            padding: 10px;
            background-color: gray;
            background-image: linear-gradient(lightgrey,white);
            font-weight:bold;
        }
        .card-header{
            background-color: gray;
            background-image: linear-gradient(lightgrey,white);
        }
        .lang{
            width: 100%;
            padding: 10px;
            outline: none;
            height:20%;
        }
        .language{
            border: 1px solid lightgrey;
            padding: 5px;
        }
        .buttons{
            background-color:#DF6C0E ;
            color:white;
            width:20%;
            height:30%;
            margin-left: 80%;
        }
        .buttons:hover{
            background-color:#DF6C0E ;
            color:white;
        }
        .forgetbttn{
            background-color:#DF6C0E ;
            color:white;
            width:50%;
            height:20%;
            margin-left: 25%;
            height:20%;

        }
        .forgetbttn:hover{
            background-color:#DF6C0E ;
            color:white;
        }

        .input-container {
            display: -ms-flexbox; /* IE10 */
            display: flex;
            width: 100%;
            margin-bottom: 15px;
        }

        .text{
            padding: 10px;
            background: lightgray;
            /*color: white;*/
            width: 80%;
            text-align: left;



        }

        .input-field {
            width: 100%;
            padding: 10px;
            outline: none;
            height:15%;
            font-size: 12px;

        }
        #figure{
            background-color: #DF6C0E;
            top: 0;
            margin-left: 0%;
            width: 50%;
            height: 100%;
            position: absolute;

        }
        .forget{
            margin-top: 3%;
        }
        .error{
            color:red;
            margin-left:50%;
            margin-top: -4%;
        }



        .navbar-brand{
            font-size: 16px;
        }

        .nav-link{
            font-size: 14px;
        }





    </style>
</head>
<body>
    <div id="app">
        {{--<nav class="navbar navbar-expand-md navbar-light navbar-laravel">--}}
            {{--<div class="container">--}}
                {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
                    {{--{{ config('app.name', 'Laravel') }}--}}
                {{--</a>--}}
                {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
                    {{--<span class="navbar-toggler-icon"></span>--}}
                {{--</button>--}}

                {{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
                    {{--<!-- Left Side Of Navbar -->--}}
                    {{--<ul class="navbar-nav mr-auto">--}}

                    {{--</ul>--}}

                    {{--<!-- Right Side Of Navbar -->--}}
                    {{--<ul class="navbar-nav ml-auto">--}}
                        {{--<!-- Authentication Links -->--}}
                        {{--@guest--}}
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                            {{--</li>--}}
                        {{--@else--}}
                            {{--<li class="nav-item dropdown">--}}
                                {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                                    {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                                {{--</a>--}}

                                {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                                    {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                        {{--{{ __('Logout') }}--}}
                                    {{--</a>--}}

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--@csrf--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--@endguest--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</nav>--}}

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>


