@extends('Admin.courses')
@section('course')
    <form class="form-horizontal" action="">
        <div class="form-group">
            <label class="control-label col-sm-3" for="pwd">Folder Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="foldername"name="foldername">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pwd">Existing Folder</label>
            <div class="col-sm-9">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Existing Folder
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>
    </form>
@endsection