@extends('Admin.courses')
@section('course')
   <div class="folderlist">
   @foreach($data->data as $datas)

{{$datas->title}}<br>
      @endforeach
      </div>
      <div class="pagination ">
         <ul class="pagination">
            <li class="{{ $data->current_page == 1 ? 'disabled' : '' }}"><a href="{{route('folderlist',['page'=>1])}}"><span>&laquo;</span></a></li>
            @for($i=1;$i<=$data->last_page;$i++)
               @if($i == 1)
                  <li class="{{ $data->current_page == 1 ? 'disabled' : '' }}"><a href="{{route('folderlist',['page'=>($data->current_page-1)])}}"><span>&lsaquo;</span></a></li>
               @endif

               <li class="{{ $data->current_page == $i ? 'active': '' }}"><a href="{{route('folderlist',['page'=>$i])}}">{{$i}}</a></li>

               @if($i == $data->last_page)
                  <li class="{{ $data->current_page == $data->last_page  ? 'disabled' : '' }}"><a href="{{route('folderlist',['page'=>($data->current_page+1)])  }}"><span>&rsaquo;</span></a></li>
               @endif
            @endfor
            <li class="{{ $data->current_page == $data->last_page  ? 'disabled' : '' }}"><a href="{{route('folderlist',['page'=>($data->last_page)])  }}"><span>&raquo;</span></a></li>
         </ul>
      </div>



    @endsection