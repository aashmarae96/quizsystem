@extends('dashboard')
@section('content')
    <style>
        .inputwidth{
            width: 30%;
            margin-bottom: 1%;

        }
        /*.selectwidth{*/
            /*width:37%;*/
        /*}*/
        .name{
            margin-left: 2%;
            font-weight: 100;
            font-size: 13px;
        }
        .addtext{
            /*padding-left: 4%;*/
            /*border-bottom: 1px solid #8B3F00;*/

        }
        .folderlist{
            padding-left: 2%;
        }

    </style>
<div class="row col-md-12">
    <div class="col-md-4 container chapterlist ">
        {{--<ul class="list-group ">--}}
       {{--@foreach($data as $datas)--}}

             {{--<a href="/exam/{{$datas->course_id}}" style="color:#8B3F00"> {{$datas->title}}</a><br>--}}

        {{--@endforeach--}}
        {{--</ul>--}}
        @include('Exam.examcourses');
    </div>

    <div class="row col-md-8 container adddata">
        <div class="row">
            {{--<nav class="navbar">--}}
                {{--<div >--}}

                    {{--<ul class="nav navbar-nav">--}}

                        {{--<li><a href="/addfolder">Add Folder</a></li>--}}
                        {{--<li><a href="/addsubfolder">Add SubFolder</a></li>--}}
                        {{--<li><a href="/folderlist">Folder List</a></li>--}}
                        {{--<li><a href="/addcourse">Add Course</a></li>--}}
                        {{--<li><a href={{route('courselist',['page'=>1])}}>Course List</a></li>--}}
                        {{--<li><a href="/courselist"> Course List</a></li>--}}

                    {{--</ul>--}}
                {{--</div>--}}
            {{--</nav>--}}
            @include('Admin.adddata')
        </div>
        <div class="row">
            @yield('course')
        </div>




</div>
</div>


    @endsection