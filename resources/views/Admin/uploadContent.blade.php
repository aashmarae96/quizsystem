@extends('Admin.courses')
@section('course')
    <form class="form-horizontal" action="/contentstore" method="POST" enctype='multipart/form-data'>
    {{ csrf_field()}}
        <div class="form-group">
            <label class="col-sm-3 name" >Choose Folder</label>
            <div class="col-sm-9 inputwidth">

                <select name="chapterName" class="form-control">
                    @foreach($data as $var)
                        <option value="{{$var->chapter_id}}" >{{$var->title}}</option>

                    @endforeach

                </select>

            </div>
        </div>
        <div class="form-group">
            <label class=" col-sm-3 name">Upload File</label>
            <div class="col-sm-9 inputwidth">
            <input  class="file-upload-input" type='file' name="fileName">
                @if ($errors->has('fileName'))
                    <div class="error">{{ $errors->first('fileName') }}</div>
                @endif
        </div>
        </div>
        <div class="form-group">
            <label class=" col-sm-3 name" >Video Url</label>
            <div class="col-sm-9 inputwidth">
                <input type="text" class="form-control" name="urlName">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default buttons">Save</button>
            </div>
        </div>
    </form>

    @endsection