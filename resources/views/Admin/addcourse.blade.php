@extends('Admin.courses')
@section('course')



        <form class="form-horizontal" action="/coursestore" method="POST" enctype='multipart/form-data'>
            {{ csrf_field()}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label class="col-sm-3 name" for="email">Create New Course</label>
                <div class="col-sm-9 inputwidth">
                    <input type="text" class="form-control" id="newcourse" name="newcourse">
                </div>
            </div>
            <div class="form-group">
                <label class=" col-sm-3 name" for="pwd">Description</label>
                <div class="col-sm-9 inputwidth">
                    <input type="textarea" class="form-control" id="description"name="description">
                </div>
            </div>
            <div class="form-group">
                <label class=" col-sm-3 name" >Price</label>
                <div class="col-sm-9 inputwidth">
                    <input type="textarea" class="form-control" id="price"name="price">
                </div>
            </div>
            <div class="form-group">
                <label class=" col-sm-3 name" >Choose Folder</label>
                <div class="col-sm-9 inputwidth">

                {{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{$data[0]->title}}--}}
                    {{--<span class="caret"></span></button>--}}
                {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="#">HTML</a></li>--}}
                    {{--<li><a href="#">CSS</a></li>--}}
                    {{--<li><a href="#">JavaScript</a></li>--}}

                    <select name="srtitle" class="form-control">
                        @foreach($data as $var)
                    <option  value="{{$var->structure_id}}" >{{$var->title}}</option>

                        @endforeach

                    </select>

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default buttons">Save</button>
                </div>
            </div>

        </form>

    @endsection('course')