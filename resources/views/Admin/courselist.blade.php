@extends('Admin.courses')
@section('course')
    <div>

    <table id="courses">
        <tr>
            <th>Courses</th>
            <th>Actions</th>
        </tr>
        {{--{{dd($data)}}--}}
        @foreach($data->data as $course)
   <tr>
       <td> {{$course->title}}</td>
       <td><a href="/edit/{{$course->course_id}}" style="color: #8B3F00"><i class="fa fa-pencil-square-o " aria-hidden="true"></i></a>
           {{--<a href="/delete/{{$course->course_id}}" style="color: #0000F0"><button type="submit">Delete</button></a>--}}
           @if($course->status==1)
           <a href="/hide/{{$course->course_id}}" style="color: #8B3F00"><i class="fa fa-eye " aria-hidden="true"></i></a>

               @else
               <a href="/hide/{{$course->course_id}}" style="color: #8B3F00"><i class="fa fa-eye-slash " aria-hidden="true"></i></a>
               @endif
       </td>
   </tr>


    @endforeach

    </table>

    </div>
    <div class="pull-right pagination">
        <ul class="pagination">
            <li class="{{ $data->current_page == 1 ? 'disabled' : '' }}"><a href="{{route('courselist',['page'=>1])}}"><span>&laquo;</span></a></li>
            @for($i=1;$i<=$data->last_page;$i++)
                @if($i == 1)
                    <li class="{{ $data->current_page == 1 ? 'disabled' : '' }}"><a href="{{route('courselist',['page'=>($data->current_page-1)])}}"><span>&lsaquo;</span></a></li>
                @endif

                <li class="{{ $data->current_page == $i ? 'active': '' }}"><a href="{{route('courselist',['page'=>$i])}}">{{$i}}</a></li>

                @if($i == $data->last_page)
                    <li class="{{ $data->current_page == $data->last_page  ? 'disabled' : '' }}"><a href="{{route('courselist',['page'=>($data->current_page+1)])  }}"><span>&rsaquo;</span></a></li>
                @endif
            @endfor
            <li class="{{ $data->current_page == $data->last_page  ? 'disabled' : '' }}"><a href="{{route('courselist',['page'=>($data->last_page)])  }}"><span>&raquo;</span></a></li>
        </ul>
    </div>

@endsection