@extends('Admin.courses')
@section('course')
<div class="row col-md-12">
    <form  action="/update/{{$data->course_id}}" method="POST" enctype='multipart/form-data'>
        {{ csrf_field()}}
        <div class="form-group row">
            <label for="coursename" class="col-sm-2 name">Course Name</label>
            <div class="col-sm-10 inputwidth">
                <input type="text" class="form-control" name="title" value="{{$data->title}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 name">Description</label>
            <div class="col-sm-10 inputwidth">
                <input type="text" class="form-control" name="description" value="{{$data->description}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 name">Price</label>
            <div class="col-sm-10 inputwidth">
                <input type="text" class="form-control" name="price" value="{{$data->price}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default buttons">Update</button>
            </div>
        </div>
    </form>
</div>



    {{--<form action="/update/{{$data->id}}" method="POST" enctype='multipart/form-data'>--}}
            {{--{{ csrf_field()}}--}}
            {{--Course Name:<input type="text" name="course" value="{{$data->title}}"><br>--}}
            {{--Description:<input type="text" name="description"value="{{$data->description}}"><br>--}}
            {{--Price:<input type="text" name="price" value="{{$data->price}}"><br>--}}
            {{--<button type="submit">Update</button>--}}
        {{--</form>--}}
    @endsection