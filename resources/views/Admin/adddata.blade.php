<nav class="navbar">
    <div >

        <ul class="nav navbar-nav">

            <li><a href="/addfolder"><i class="fa fa-folder-open"></i> Add Folder</a></li>
            {{--<li><a href="/addsubfolder">Add SubFolder</a></li>--}}
            <li><a href="/folderlist"><i class="fa fa-folder"></i> Folder List</a></li>
            <li><a href="/addcourse"><i class="fa fa-plus-square"></i> Add Course</a></li>
            {{--<li><a href={{route('courselist',['page'=>1])}}>Course List</a></li>--}}
            <li><a href="/courselist"> <i class="fa fa-list-alt"></i> Course List</a></li>
            <li><a href="/uploadcontent"><i class="fa fa-upload"></i> Upload Content</a> </li>

        </ul>
    </div>
</nav>