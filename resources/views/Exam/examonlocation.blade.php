@extends('Exam.exams')
@section('exam')
    <div>
        <button class="btn btn-default">List Of Exam</button>
        <button class="btn btn-default">Exam Result</button>
        <button class="btn btn-default">Exam Course</button>
    </div>
    <div>
        <form class="form-horizontal" action="/examstore" method="POST" enctype='multipart/form-data'>
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label col-sm-3" for="Exam Name">Exam Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="examname">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Choose Chapter</label>
                <div class="col-sm-9">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Chapter
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                    </ul>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Date</label>
                <div class="col-sm-9">
                    <select name="day">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                    <select name="month">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                    <select name="year">
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Time</label>
                <div class="col-sm-9">
                    <input type="text"name="hour" style="width:10%">Hour
                    <input type="text"name="minute" style="width:10%">Minute
                    <input type="text"name="second" style="width:10%">Second

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="Exam Name">Location</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="examname">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Save</button>
                </div>
            </div>



        </form>
    </div>
    @endsection