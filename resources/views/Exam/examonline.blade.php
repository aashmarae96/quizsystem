@extends('Exam.exams')
@section('exam')




<div>
    <button class="btn btn-default">Exam Result</button>
    <button class="btn btn-default">Exam Course</button>
</div>
<div>
    <form class="form-horizontal" action="/examstore" method="POST" enctype='multipart/form-data'>
        {{ csrf_field()}}
        <div class="form-group">
            <label class="control-label col-sm-3" for="Exam Name">Exam Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="examname">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Choose Chapter</label>
            <div class="col-sm-9">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Chapter
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" >Category</label>
            <div class="col-sm-9">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Category
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" >Subject</label>
            <div class="col-sm-9">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Subject
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">No of Questions</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="numquestion">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Correct Answer</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="correctans">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Exam Time</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="correctans">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>




    </form>
</div>

    @endsection


