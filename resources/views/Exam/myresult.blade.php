<html>
<head>
    <style>
        div.content{
            margin-top: 5%;
        }
        #courses {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;

        }
        #courses td, #courses th {
            border: 1px solid #ddd;
            padding: 8px;
        }
        #courses tr:nth-child(even){background-color: #F8DDC2;}

        #courses tr:hover {background-color: #ddd;}

        #courses th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color:  #FB6203;
            color: white;
        }

    </style>
</head>
<body>

<script type="text/php">
    if (isset($pdf)) {

        $size = 8;
        $string="Printed by: UMVA ";
        $string1="AUX-EL";
        $text = date('Y-m-d');
        $font = $fontMetrics->getFont("helvetica");
        $text_height = $fontMetrics->get_font_height($font, $size);
        $width = $fontMetrics->get_text_width($text, $font, $size);
        $w = $pdf->get_width() - $width - 24;
        $y = $pdf->get_height() - $text_height - 24;
        $a = $pdf->get_width();
        $b = $pdf->get_height()-99/100*$pdf->get_height();

        $page=$pdf->page_text($a/2,$y,"{PAGE_NUM}/{PAGE_COUNT}", $font,$size, array(0,0,0));
        $pdf->page_text($a-20/100*$a,$b,"Created on:$text",$font, $size);
        $pdf->page_text($a-17/100*$a,$y,$string,$font,$size);
        {{--$pdf->page_text(10,$b,$string1,$font,$size);--}}

        $header = $pdf->open_object();
        $pdf->image("img/auxel-logo.png",200,10,152,46,"normal");
        $pdf->close_object();
        $pdf->add_object($header,"all");


        $footer=$pdf->open_object();
        $pdf->image("img/umva.png",$a-24/100*$a,$y,40,20,"normal");
        $pdf->close_object();
        $pdf->add_object($footer,"all");

    }

</script>


{{--<p style="text-align:center"><img src="img/auxel-logo.png"></p>--}}
<div class="content">
{{--@foreach($data as $key=>$value)--}}
    {{--@if($key=='correct')--}}
        {{--Correct Answer ::{{$value}}<br>--}}
        {{--@else--}}
        {{--Incorrect Answer ::{{$value}}--}}
        {{--@endif--}}
    {{--@endforeach--}}
    <table id="courses">
        <tr>
            <th>Correct</th>
            <th>Incorrect</th>
            <th>Percentage</th>
        </tr>
        <tr>
            <td>{{$correct}}</td>
            <td>{{$incorrect}}</td>
            <td>{{$percentage}}</td>
        </tr>

    </table>
    {{--Correct answer={{$correct}};--}}
    {{--Incorrect answer={{$incorrect}};--}}
</div>



</body>
</html>