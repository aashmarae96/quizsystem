@extends('Exam.exams')
@section('exam')
    <div>
        <form class="form-horizontal" action="/upload" method="POST" enctype='multipart/form-data'>
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label col-sm-3" for="Exam Name">Upload Question</label>
                <div class="col-sm-9">
                    <input type="file"  name="fileToUpload" id="fileToUpload">

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Upload</button>
                </div>
            </div>


        </form>
    </div>
    @endsection