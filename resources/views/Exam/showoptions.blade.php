
@extends('dashboard')
@section('content')
    <div class="row col-md-12">
        <div class="col-md-4">
<div>
    <p style="font-size:20px"><b>Read PDF</b></p>

    @foreach($var as $data)

        {{$data->filename}}  <a href="{{ asset('/storage/PDF/'.$data->filename) }}" download="{{$data->filename}}" style="color:black"><button  class="btn btn-default">Download</button></a><br>
        @if($data->video_url!=null)
      @php( preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $data->video_url, $matches) )
      @php($vid = $matches[1])
        <iframe id="ytplayer" type="text/html" width="400px" height="250px"
                src="https://www.youtube.com/embed/{{$vid}}"
                frameborder="0" allowfullscreen></iframe>
        @endif
    @endforeach

</div>
            <a href="/examqsn/{{$id}}"><button  class="btn btn-default">Give Exam</button></a>

    </div>

    </div>
@endsection
