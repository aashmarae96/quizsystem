<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/course','API\CourseController@show');
Route::get('/courselist','API\CourseController@showallcourses');
Route::post('/newcourse','API\CourseController@store');
Route::get('/showcourse','API\CourseController@showcourse');
Route::post('/getdata','API\EditController@index');
Route::post('/updating','API\EditController@update');
Route::post('/status','API\EditController@status');
Route::get('/courseslist','API\ExamController@index');
Route::get('/examshow','API\ExamController@show');
Route::any('/checkinganswer','API\AnswerController@checkanswer');

Route::get('/content','API\ContentController@index');     //uploading content
Route::post('/contentstore','API\ContentController@store');

Route::get('/fetchData','API\ContentController@fetchData');
Route::get('/folderlist','API\NavController@folderList');


Route::post('/login','API\UserController@check');