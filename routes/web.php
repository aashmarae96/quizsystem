<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

    Route::get('/', function () {

        return view('welcome');
    });
    Route::get('/home', 'Web\HomePageController@index');
    Route::get('redirect',function(){

        $query = http_build_query([
        'client_id' => '7',
        'redirect_uri' => 'http://localhost:8117/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://localhost:8000/oauth/authorize?'.$query);
    })->name('get.token');

    Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://localhost:8000/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '7',
            'client_secret' => 'aqtnG0jhCEi9WvfSDwVkBXAgW6Paz8x5voLJBxaY',
            'redirect_uri' => 'http://localhost:8117/callback',
            'code' => $request->code,
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});
    Auth::routes();



    Route::post('/loginCheck','Web\UserController@homePage');
    Route::get('/dashboard','Web\DashboardController@index');

    Route::get('/about', function () {
        return view('aboutpage');
    });


//Route for user part

//Route::get('/overview','Web\CourseController@show');

    Route::get('/overview', 'Web\ExamController@index');
    Route::get('/myauxel', 'Web\MyAuxelController@show');
    Route::get('/about', function () {
        return view('Aboutpage');
    });
//    Route::get('/dashboard', function () {
//        return view('dashboard');
//    });

    Route::get('/logout','Web\DashboardController@logout');
    Route::post('/coursestore', 'Web\CourseController@store');
    Route::get('/addfolder', 'Web\NavController@addfolder');
    Route::get('/addsubfolder','Web\NavController@addsubfolder');
    Route::get('/folderlist', 'Web\NavController@folderlist')->name('folderlist');
    Route::get('/addcourse', 'Web\CourseController@show');
    Route::get('/courselist', 'Web\CourseController@showcourse')->name('courselist');
    Route::get('/edit/{id}', 'Web\EditController@index');
    Route::get('/exams', function () {
        return view('Admin.exams');
    });

    Route::post('/update/{id}', 'Web\EditController@update');

    Route::get('/hide/{id}', 'Web\EditController@status');

    Route::get('/exam', function () {
        return view('Exam.exams');
    });

    Route::get('/examonline', function () {
        return view('Exam.examonline');
    });

    Route::get('/examonlocation', function () {
        return view('Exam.examonlocation');
    });

    Route::get('/exampractice', function () {
        return view('Exam.exampractice');
    });

    Route::get('/uploadquestion', function () {
        return view('Exam.uploadquestions');
    });

    Route::get('/video','VideoController@index');

//    Route::get('/exam/{id}', 'Web\ExamController@show');
//Route::get('/exam/{id}','Web\ExamController@displayData');
    Route::get('/exam/{id}','Web\ContentController@fetchData');
    Route::get('/examqsn/{id}','Web\ExamController@show');

    Route::post('/checkanswer', 'Web\AnswerController@check');
    Route::post('/finaldata', 'Web\AnswerController@store');

//    For uploading content

    Route::get('/uploadcontent','Web\ContentController@index');
    Route::post('/contentstore','Web\ContentController@store');
    Route::get('read/{id}','Web\ContentController@fetchPDF');
    Route::get('watch/{id}','Web\ContentController@fetchVideo');


    Route::post('/downloadResult','Web\ResultController@index');






