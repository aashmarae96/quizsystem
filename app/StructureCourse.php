<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StructureCourse extends Model
{
    protected $table="structure_course";
    public $timestamps= false;
}
