<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    protected $table="structure";
//    protected $fillable=['type','title'];

    public function course(){
        return $this->belongsToMany('App\Course');
    }
}
