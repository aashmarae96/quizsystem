<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectiveAnswer extends Model
{
    protected $table="objectives_answer";
    protected $primaryKey = 'answer_id';
    public $timestamps= false;

    public function objectivesquestion(){
        return $this->belongsTo('App\ObjectiveQuestion');
    }
}
