<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectiveQuestion extends Model
{
    protected $table="objectives_question";
    public $timestamps= false;

    public function objectivesanswer(){
        return $this->hasMany('App\ObjectivesAnswer');
    }
}
