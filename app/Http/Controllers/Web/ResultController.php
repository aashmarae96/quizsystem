<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function index(Request $request)
    {
        $percentage=0;
        $correct=0;
        $incorrect=0;


        $data=$request->data;
//        dd($data);
        foreach ($data as $key=>$value){
            if($key=='correct'){
              $correct=$value;
            }
            else if($key=='incorrect'){
                $incorrect=$value;
            }
            else{
               $percentage=$value;

            }
        }

        $pdf=PDF::loadView('Exam.myresult',compact('correct','incorrect','percentage'));
        $pdf->setPaper('A4', 'portrait');
        return $pdf->download('myresult.pdf');
    }
}
