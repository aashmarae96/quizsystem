<?php

namespace App\Http\Controllers\Web;

use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyAuxelController extends Controller
{
    use CourseTrait;
    public function show(Request $request){
//        $value=$request->session()->get('user');

        return view('MyAuxel',compact('value'));
    }
}
