<?php

namespace App\Http\Controllers\Web;

use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    use CourseTrait;
    public function index(){
        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('GET', 'content');

        $data=json_decode($res->getBody());
//       dd($data);
        $list=$this->display();
        return view('Admin.uploadContent',compact('data','list'));

    }
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'fileName' => 'required|mimes:pdf',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }


            $filenameWithExt = $request->file('fileName')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileName')->getClientOriginalExtension();
            //filename
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            //dd($filenameToStore);
            $path = $request->file('fileName')->storeAs('public/PDF', $filenameToStore);

        $id=$request->chapterName;
       //dd($chapterName);
        $urlName=$request->urlName;
//        dd($urlName);

        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('POST', 'contentstore',[
                'form_params' => [
                    'chapterid'=>$id,
                    'fileName'=>$filenameToStore,
                    'urlName'=>$urlName
                ]]
        );
        $var=json_decode($res->getBody()->getContents());
        dd($var);

    }
//    public function fetchPDF(Request $request,$id){
//        $client=New Client();
//        $res=$client->request('GET','http://127.0.0.1:8016/api/fetchPDF',[
//            'query'=>[
//                'id'=>$id
//            ]
//
//        ]);
//        $var=json_decode($res->getBody()->getContents());
//        dd($var);
//
//
//
//    }
//    public function fetchVideo(Request $request,$id){
//        $client=New Client();
//        $res=$client->request('GET','http://127.0.0.1:8016/api/fetchVideo',[
//            'query'=>[
//                'id'=>$id
//            ]
//
//        ]);
//        $var=json_decode($res->getBody()->getContents());
//        dd($var);
//    }

public function fetchData(Request $request,$id){
        $client=new Client(['base_uri' => config('app.api')]);
        $res=$client->request('GET','fetchData',[
            'query'=>[
                'id'=>$id
            ]
        ]);
    $var= \GuzzleHttp\json_decode($res->getBody()->getContents());
//dd($var);
    return view('Exam.showoptions',compact('var','id'));

}
}
