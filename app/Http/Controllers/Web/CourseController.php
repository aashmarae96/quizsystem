<?php

namespace App\Http\Controllers\Web;

use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;


use App\StructureCourse;
use App\Course;
use Illuminate\Support\Facades\Session;


class CourseController extends Controller
{

    use CourseTrait;

    public function show(){

        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('GET', 'course');

        $data=json_decode($res->getBody());
        $list=$this->display();

//        dd($value);
        return view('Admin.addcourse',compact('data','list'));
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'newcourse' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',

        ]);


        $title=$request->newcourse;
        $description=$request->description;
        $price=$request->price;
       $stitle=$request->srtitle;



//dd($structureCourse);
        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('POST', 'newcourse',[
                'form_params' => [
                    'title'=>$title,
                    'description'=>$description,
                    'price'=>$price,
                   'stitle'=>$stitle
                ]]
        );
        $var=json_decode($res->getBody()->getContents());
if($var){

    return "Success";
}
else{
    return redirect('/addcourse');
}



    }
    public function showcourse(Request $request){

        $page = $request->page;
        //dd($page);
        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('GET', 'showcourse',[
            'query' => [
                'page'=>$page

            ]]);
//dd($res->getBody()->getContents());
        $list=$this->display();
        $data=json_decode($res->getBody()->getContents());
        //dd($data);
        return view('Admin.courselist',compact('data','list'));
    }

}
