<?php

namespace App\Http\Controllers\Web;
use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ExamController extends Controller
{
   use CourseTrait;
    public function index(){

        $list=$this->display();

        return view('Admin.courses',compact('list'));
    }

    public function show(Request $request){

        $id=$request->id;

        $client=new Client(['base_uri'=>config('app.api')]);
        $res=$client->request('GET','examshow',[
            'query'=>[
                'id'=>$id
            ]
        ]);
        $data=json_decode($res->getBody()->getContents());

        return view('Exam.examquestions',compact('data'));


    }
    public function displayData($id){


      return view ('Exam.showoptions',compact('id'));

    }
}
