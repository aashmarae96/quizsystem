<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CourseTrait;
use GuzzleHttp\Client;

class NavController extends Controller
{
    use CourseTrait;
    public function addfolder()
    {
        $list = $this->display();
        return view('Admin.addfolder', compact('list'));
    }
    public function addsubfolder(){
        $list=$this->display();
        return view('Admin.addsubfolder',compact('list'));

    }
    public function folderlist(Request $request){
        $page = $request->page;
        //dd($page);

        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('GET', 'folderlist',[
            'query' => [
                'page'=>$page

            ]]);

        $data=json_decode($res->getBody());
        //dd($data);
        $list=$this->display();
        return view('Admin.folderlist',compact('list','data'));
    }
//    public function folderlist(){
//
//        $client=new Client();
//        $res = $client->request('GET', 'http://127.0.0.1:8016/api/folderlist');
//
//        $data=json_decode($res->getBody());
//        $list=$this->display();
//        return view('Admin.folderlist',compact('list','data'));
//    }
}
