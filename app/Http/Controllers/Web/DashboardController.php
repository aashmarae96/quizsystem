<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class DashboardController extends Controller
{
    public function index(Request $request){


        return view('dashboard',compact('value'));
    }

    public function logout(){
        Auth()->logout();
        return redirect('/login');
    }
}
