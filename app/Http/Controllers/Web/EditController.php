<?php

namespace App\Http\Controllers\Web;

use App\Course;
use App\Traits\CourseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class EditController extends Controller
{
    use CourseTrait;
    public function index($id){
        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('POST', 'getdata',[
                'form_params' => [
                    'id'=>$id
                ]]
        );
        $data=json_decode($res->getBody());
        $list=$this->display();
        return view('admin.edit',compact('data','list'));
    }
    public function update(Request $request,$id){
        $title=$request->title;
        $description=$request->description;
        $price=$request->price;


        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('POST', 'updating',[
                'form_params' => [
                    'id'=>$id,
                    'title'=>$title,
                    'description'=>$description,
                    'price'=>$price
                ]]
        );

        $data=json_decode($res->getBody());
        if($data){
            return redirect('courselist');
        }

    }
    public function status($id){

        $client=new Client(['base_uri'=>config('app.api')]);
        $res = $client->request('POST', 'status',[
                'form_params' => [
                    'id'=>$id

                ]]
        );

        $data=json_decode($res->getBody());
        if($data){
            return redirect('courselist');
        }




    }
}
