<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


use GuzzleHttp\Client;
use App\User;

class UserController extends Controller
{

    public function homePage(Request $request)
    {


        if ($request->getMethod() == 'POST') {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required'
            ]);


            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            }
        }
        $client = new Client(['base_uri' => config('app.api')]);
        $response = $client->request('POST', 'login', [
            'form_params' => [
                'name' => $request->get('name'),
                'password' => $request->get('password')
            ]
        ]);
        $userapi = \GuzzleHttp\json_decode($response->getBody()->getContents());
        //dd($userapi);

        if (property_exists($userapi, 'error')) {

            if ($userapi->error == 404) {
                return redirect()->back()->with('status', $userapi->message);
            }
        }


        $user= $userapi->sup_name;
        $request->session()->put('user', $user);
//        $value=$request->session()->get('user');
//        dd($value);


        if ($userapi) {

           return redirect('/home');
           // return view('dashboard', compact('username'));
        }
    }
}












