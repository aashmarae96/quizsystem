<?php

namespace App\Http\Controllers\API;

use App\ObjectiveAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class AnswerController extends Controller
{
    public function checkanswer(Request $request)
    {
        $questions=$request->get('value');
        $number=count($questions);
        $percentage=0;
        $correct_count=0;
        $incorrect_count=0;


        foreach ($questions as $key => $value) {
            foreach ($value as $key1 => $data) {
                if ($key1 == 'answer_id') {
                    $answer_id[] = $data;


                } elseif ($key1 == 'correct_id') {
                    $correct_id[] = $data;
                }
            }
        }


            foreach ($answer_id as $key => $data) {

                if ($data == $correct_id[$key]) {
                   $correct_count = $correct_count + 1;
                } else {
                   $incorrect_count= $incorrect_count + 1;
                }
                $percentage=$correct_count/$number*100;




            }
       return response()->json(['correct'=>$correct_count,'incorrect'=>$incorrect_count,'percentage'=>$percentage]);
        //return response()->json($percentage);
        }


}
