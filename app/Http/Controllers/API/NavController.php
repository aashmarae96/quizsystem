<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Structure;

class NavController extends Controller
{
//    public function folderList(){
//        $data=Structure::all();
//        return response()->json($data);
//
//    }

public function folderlist(Request $request){
    $page=$request->get('page');
    //return $page;
    //return response()->json($page);
    $data=Structure::paginate(14);
    if($page){
        $data->appends($page);
    }

    return response()->json($data);
}


}
