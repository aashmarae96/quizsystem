<?php

namespace App\Http\Controllers\API;

use App\Chapter;
use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    public function index(){
        $chapter=Chapter::all();
        return response()->json($chapter);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $id=$request->chapterid;
        $fileName=$request->fileName;
        $urlName=$request->urlName;
        $data=Chapter::find($id);

        if($fileName){
            $data->filename=$request->fileName;
            $data->save();
        }
        if($urlName){
            $data->video_url=$request->urlName;
            $data->save();
        }

       if($data->save()){
           $message="Success";
           return response()->json($message);

       }
       else{
           $message="Failure";
           return response()->json($message);

       }





    }
//    public function fetchPDF(Request $request){
//        $id=$request->get('id');
//        $qsn=DB::table('chapters')
//            ->join('course_chapter','course_chapter.chapter_id','=','chapters.chapter_id')
//            ->join('courses','course_chapter.course_id','=','courses.course_id')
//            ->select('chapters.filename')
//            ->where('courses.course_id',$id)
//            ->get();
//
// return response()->json($qsn);
//
//
//    }
//
//    public function fetchVideo(Request $request){
//        $id=$request->get('id');
//        $qsn=DB::table('chapters')
//            ->join('course_chapter','course_chapter.chapter_id','=','chapters.chapter_id')
//            ->join('courses','course_chapter.course_id','=','courses.course_id')
//            ->select('chapters.video_url')
//            ->where('courses.course_id',$id)
//            ->get();
//
//        return response()->json($qsn);
//    }
public function fetchData(Request $request){
    $course_id=$request->get('id');
        $qsn=DB::table('chapters')

            ->leftJoin('course_chapter','course_chapter.chapter_id','=','chapters.chapter_id')
            ->leftJoin('courses','course_chapter.course_id','=','courses.course_id')
            ->select('chapters.filename','chapters.video_url')
            ->whereNotNull('chapters.filename')
            ->where('courses.course_id',$course_id)
            ->get();

        return response()->json($qsn);
}
}
