<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;

class EditController extends Controller
{
    public function index(Request $request){
        $data=Course::find($request->id);
        return response()->json($data);
    }

    public function update(Request $request){
//        dd($request->id);
        $data=Course::find($request->id);
        $data->title=$request->title;
//        dd($data->title);
        $data->description=$request->description;
        $data->price=$request->price;
        $data->save();
        return response()->json($data);
    }
    public function status(Request $request){
        $data=Course::find($request->id);
        if($data->status==1) {
            $data->status=0;
            $data->save();

        }
        elseif($data->status==0) {
            $data->status = 1;
            $data->save();
        }
        return response()->json($data);
    }
}
