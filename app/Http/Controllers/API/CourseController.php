<?php

namespace App\Http\Controllers\API;

use App\Structure;
use App\StructureCourse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;

class CourseController extends Controller
{
   public function show(){

       $data=Structure::all();


  return response()->json($data);

   }
   public function showallcourses(){
    $courses= Course::all();
    return response()->json([
      'data'=>$courses,
      'message'=>'Roshan Gadha ho'
    ]);
   }
   public function store(Request $request){
       //$var=$request->stitle;
       //return $request;


       $course=new Course();

       $course->title=$request->title;

       $course->description=$request->description;
       $course->price=$request->price;
       $course->status = 1;
       $course->save();

       // dd($course->course_id);

       $structureCourse = new StructureCourse();
       $structureCourse->structure_id =$request->stitle;
       $structureCourse->course_id = $course->course_id;
       $structureCourse->save();

       return response()->json($course);
   }

   public function showcourse(Request $request){
       $page=$request->get('page');
       //return $page;
       //return response()->json($page);
       $course=Course::paginate(8);
       if($page){
           $course->appends($page);
       }

       return response()->json($course);
   }


}
