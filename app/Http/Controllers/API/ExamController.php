<?php

namespace App\Http\Controllers\API;

use App\Course;
use App\ObjectiveQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CourseChapter;
use Illuminate\Support\Facades\DB;
class ExamController extends Controller
{
//    public function index(){
//        $data=Course::all();
//        return response()->json($data);
//    }
    public function show(Request $request){
        $id=$request->get('id');
        /*$ans=DB::table('objectives_question')
            ->join('course_chapter','course_chapter.chapter_id','=','objectives_question.chapter_id')
            ->join('chapters','chapters.chapter_id','=','course_chapter.chapter_id')
            ->join('courses','courses.course_id','=','course_chapter.course_id')
            ->join('objectives_answer','objectives_answer.objectives_id','=','objectives_question.objectives_id')
            ->select('objectives_answer.answer')
            ->where('courses.course_id',$id)
            ->where('objectives_answer.correct','=',1)
            ->get();*/
//
//        $qsn=DB::table('objectives_question')
//            ->join('course_chapter','course_chapter.chapter_id','=','objectives_question.chapter_id')
//            ->join('chapters','chapters.chapter_id','=','course_chapter.chapter_id')
//            ->join('courses','courses.course_id','=','course_chapter.course_id')
//            ->join('objectives_answer','objectives_answer.objectives_id','=','objectives_question.objectives_id')
//            ->select('objectives_question.questions','objectives_question.objectives_id')
//            ->selectRaw('GROUP_CONCAT(objectives_answer.answer ) as answer')
//            ->selectRaw('GROUP_CONCAT(objectives_answer.answer_id  ) as answer_id')
//            ->selectRaw('GROUP_CONCAT(objectives_answer.correct ) as correct')
//            ->where('courses.course_id',$id)
//            ->groupBy('objectives_question.questions','objectives_question.objectives_id')
//            ->get();
//        return response()->json($qsn);

        $qsn=DB::table('objectives_question')
            ->join('course_chapter','course_chapter.chapter_id','=','objectives_question.chapter_id')
            ->join('chapters','chapters.chapter_id','=','course_chapter.chapter_id')
            ->join('courses','courses.course_id','=','course_chapter.course_id')
            ->join('objectives_answer','objectives_answer.objectives_id','=','objectives_question.objectives_id')
            ->select('objectives_question.questions','objectives_question.objectives_id',
            DB::raw("(GROUP_CONCAT(objectives_answer.answer SEPARATOR '*')) as answer "))
            ->selectRaw('GROUP_CONCAT(objectives_answer.answer_id  ) as answer_id')
            ->selectRaw('GROUP_CONCAT(objectives_answer.correct ) as correct')
            ->where('courses.course_id',$id)
            ->groupBy('objectives_question.questions','objectives_question.objectives_id')
            ->get();
        return response()->json($qsn);


    }
}
