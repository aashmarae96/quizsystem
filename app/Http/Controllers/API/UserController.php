<?php

namespace App\Http\Controllers\API;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function check(Request $request)
    {


        $credentials = $request->only(['name', 'password']);
        $password = md5($request->password);
        $validator = Validator::make($credentials, [
            'name' => 'required',
            'password' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors());
        }


        $apiToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.InVtdmEi.6FKdx5OPZ8knwOeMUTjT-M2j-zG0OD8ntRnpvXXr0VA';
        $client = new Client(['base_uri' => config('app.test')]);
        $response = $client->request('GET', 'custom_ValidateLoginUser', [
            'query' => [
                'token' => $apiToken,
                'user_id' => $request->name,
                'role_id' => 0,
                'password' => md5($request->password),
                'namespace' => 'frontend',
                'passphrase' => ''
            ]
        ]);
        $userData = \GuzzleHttp\json_decode($response->getBody()->getContents());
         //return response()->json($userData);


        if (array_key_exists('user_id', $userData)) {
            $userId = $userData->user_id;
            $userResponse = $client->request('GET', 'generated_getUser', [
                'query' => [
                    'token' => $apiToken,
                    'user_id' => $userId
                ]
            ]);


            $users = $userResponse->getBody()->getContents();
            $decodeUser = \GuzzleHttp\json_decode($users);
            $user = $decodeUser->{'0'};
            return response()->json($user);

        }
        else {
            return response()->json($userData);
        }

    }

}

