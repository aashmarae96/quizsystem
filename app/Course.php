<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $primaryKey = "course_id";
    protected $fillable=['title','description','price','status'];
    protected $table="courses";
    public $timestamps=false;


    public function structure(){
        return $this->BelongsToMany('App\Structure');
    }

    public function chapter(){
        return $this->BelongsToMany('App\Chapter');
    }
}
